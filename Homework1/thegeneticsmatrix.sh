#!/bin/bash

filename=$(basename hapmap1.ped);
#basename takes the file name and prints the last component of the filename, here hapmap1.ped is fed into filename

size=$(<$filename wc -l);
#wc-l outputs the number of lines in hapmap1.ped to the variable 'size'. '<' is used as a input redirector 

#iterating through the lines in hapmap1.ped using a for loop
for i in `seq 1 $size`
do
  tail -${i} hapmap1.ped #tail command to display line by line
  sleep 1; #delay of 1 second before the next line is displayed 
done


