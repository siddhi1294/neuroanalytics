#!/bin/bash 

#Creating a new directory all__files1
mkdir all__files1
for i in `seq 1 50`
#iterating through the donors 
do      
 mkdir faked__datas${i}
 #creating a new directory for each donor    
 for j in `seq 1 10`
#iterating through the timepoints for each donor 
  do 
   
   touch all__files1/donor${i}_tp${j}.txt; 
#Creating files for each donor at each time point 
    
   echo "data" >> all__files1/donor${i}_tp${j}.txt; 
#writing the data header to each of the files 
   for k in {1..5} 
    do  
      echo $RANDOM >> all__files1/donor${i}_tp${j}.txt;
#generating 5 random numbers and writing it to each file     
     mv all__files1/donor${i}_tp${j}.txt faked__datas${i}/donor${i}_tp${j}.txt;
#moving each donor's file to their own directory
    done 
 done 
done 
