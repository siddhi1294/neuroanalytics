#!/bin/bash 

#Creating a new directory fileseries 10 
mkdir fileseries10 
for i in `seq 1 50`#iterating through the donors 
do         
 for j in `seq 1 10`#iterating through the timepoints for each donor 
  do 
   touch fileseries10/donor${i}_tp${j}.txt; #Creating files for each donor at each time point 
   echo "data" >> fileseries10/donor${i}_tp${j}.txt; #writing the data header to each of the files 
   for k in {1..5}
    do  
      echo $RANDOM >> fileseries10/donor${i}_tp${j}.txt;#generating 5 random numbers and writing it to each file 
    done 
 done 
done 
