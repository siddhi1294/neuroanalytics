#!/bin/bash

#Spinning design 1
for run in {1..10}; #10 revolutions 
do
##Make 1 revolution in anti-clockwise direction  
  echo -ne '\\';#Display'\', -n omits the trailing new line after the output, -e enables interpretation of escape characters
  sleep 0.2;#pause for 0.2 second 
  echo -ne '\b-';#backspace and display'-'
  sleep 0.2;#pause for 0.2 second 
  echo -ne '\b/';#backspace and display'/'
  sleep 0.2;#pause for 0.2 second 
  echo -ne '\b|';#backspace and display'|'
  sleep 0.2;#pause for 0.2 second 
  echo -ne '\b/';#backspace and display '/'##make 1 revolutions of spinner in a clockwise direction 
  sleep 0.2;#pause for 0.2 second 
  echo -ne '\b-';#backspace and display '-'
  sleep 0.2;#pause for 0.2 second
  echo -ne '\b\\';#backspace and display '\'
  sleep 0.2;#pause for 0.2 second 
  echo -ne '\b|';#backspace and display '|'
  sleep 0.2;#pause for 0.2 second
  echo -ne '\b';#backspace 
done 
  echo -ne '\b \n';#backspace and add a new line to the output 

#Spinning design 2
for run in {1..10}; #10 revolutions
do
 for run in {1..2}; 
 do 
##Make 2 revolutions in anti-clockwise direction
  echo -ne '\\';#Display'\', -n omits the trailing new line after the output, -e enables interpretation of escape characters
  sleep 0.1;#pause for 0.1 second 
  echo -ne '\b-';#backspace and display'-'
  sleep 0.1;#pause for 0.1 second 
  echo -ne '\b/';#backspace and display'/'
  sleep 0.1;#pause for 0.1 second 
  echo -ne '\b|';#backspace and display'|'
  sleep 0.1;#pause for 0.1 second 
  echo -ne '\b';#backspace
 done 
 for run in {1..2};
 do
##Make 2 revolutions in clockwise direction 
  echo -ne '\b/';#backspace and display '/' 
  sleep 0.1;#pause for 0.2 second 
  echo -ne '\b-';#backspace and display '-'
  sleep 0.1;#pause for 0.2 second
  echo -ne '\b\\';#backspace and display '\'
  sleep 0.1;#pause for 0.2 second 
  echo -ne '\b|';#backspace and display '|'
  sleep 0.1;#pause for 0.2 second
  echo -ne '\b';#backspace 
 done
done  
  echo -ne '\b \n';#backspace and add a new line to the output 
  
 
