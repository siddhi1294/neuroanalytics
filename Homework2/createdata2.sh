#!/bin/bash

for donor in `seq 1 100` #iterating through the 100 donors
do
    DONOR=$(printf "%03d" $donor); #adding leading zeros, %03d states that the number should consist of minimum 3 digits and thus it will put the missing leading zeros 
				
    for tp in `seq 1 10` #iterating through the 10 timepoints 
    do
       TP=$(printf "%03d" $tp); #adding leading zeros to timepoints 

      	echo "data" > donor${DONOR}_tp${TP}.txt; #adding a data header to each file 
	echo $RANDOM >> donor${DONOR}_tp${TP}.txt;#adding 5 random numbers to each file 
	echo $RANDOM >> donor${DONOR}_tp${TP}.txt;
	echo $RANDOM >> donor${DONOR}_tp${TP}.txt;
	echo $RANDOM >> donor${DONOR}_tp${TP}.txt;
    done
done
